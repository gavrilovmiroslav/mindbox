
(function(Mindbox) {

	/*
	 Mindbox DOM tree class
	*/
	var DomTree = function(environment) {
		var ripIdAndClass = function(title) {
			return title
				.replace(/#/gi, "_id_")
				.replace(/\./gi, "_class_");
		}

		var domtree_restart = function() {
			$("#mindbox-domtree__body").html("<strong>body</strong>");
		};

		var domtree_update = function(parent, child) {
			var $parent = $("#mindbox-domtree__" + ripIdAndClass(parent));
			var $child = $("<div class='node' id='mindbox-domtree__" + ripIdAndClass(child) + "' data-selector='" + child + "'>" + child + "</div>");
			$child.on('click', function() {				
				Mindbox.Context.setSelector(child);
			});
			$parent.append($child);
		};

		void function initialize() {
			environment.triggers.onStructureUpdated.push(domtree_update);
			environment.triggers.onStructureRestarted.push(domtree_restart);

			$(function() {
				$("html").append("<div id='mindbox-domtree'><div id='mindbox-domtree__body'><strong>body</strong></div></div>");

				$(document).keypress(function(ev) {
					if(ev.which === 96) {  // tilde
						$("#mindbox-domtree").toggle();
						if($("#mindbox-domtree").is(":visible")) {
							$("#mindbox-domtree .input").focus();
						}

						ev.preventDefault();
						return false;
					}
				});
			});
		}();

		return {			
		};		
	};

	Mindbox.DomTree = DomTree;
})(Mindbox);
