(function(Mindbox, MindboxContext, FirebaseSelectorTransformation) {

	/*
	 Mindbox console class
	*/
	var Console = function(environment) {

		// aux function: creates a family of functions useful
		// for splitting strings so that the remainder of the
		// split string (after the limit) forms one single entry
		var splitBy = function(delimiter) {
			return function(string, limit) {
				if(limit === undefined) {
					return string.split(delimiter);
				} else {
					var tokens = string.split(delimiter);
					var head = tokens.splice(0, limit);
					head.push(tokens.join(delimiter));
					return head;
				}
			};
		};

		var getCurrentSheet = function() {
			return MindboxContext.sheet;
		};

		var getCurrentSelector = function() {
			return MindboxContext.selector;
		};

		var setPrompt = function() {
			$("#mindbox-console .prompt").text(
				"[ " + MindboxContext.sheet + " ] " + 
				FirebaseSelectorTransformation.get(MindboxContext.selector) 
				+ " >");
		};

		var interpretInput = function(input) {
			if(input[input.length - 1] === ";") {
				input = input.substring(0, input.length - 1);
			}

			var split = splitBy(" ");
			var splitRule = splitBy(":");
			
			input = input.trim();
			var tokens;
			switch(input[0]) {
				// $<selector>
				case ":":
					MindboxContext.setSelector(FirebaseSelectorTransformation.set(input.substring(1).replace(/;/gi, "")));
					//setPrompt();
				break;

				// +<selector>[, <selector>]*
				case "+":
					if(!MindboxContext.selector) {
						throw "No selector given.";
					}

					var $parent = $(FirebaseSelectorTransformation.get(MindboxContext.selector));

					input = input.substring(1);
					var elements = input.split(",")
						.map(function(entry) {
							return FirebaseSelectorTransformation.set(entry.trim());
						}).forEach(function(entry) {
							environment
								.structure
								.child(MindboxContext.selector)
								.push(entry);
						});
				break;

				// *<style name> <style media query>
				case "*":
					tokens = split(input, 1);
					var styleName = tokens[0].substring(1);
					var styleMediaQuery = tokens[1];
					
					MindboxContext.sheet = styleName;
					if(styleName && styleMediaQuery) {
						environment.sheets.child(styleName).set({
							query: styleMediaQuery,
							styles: {}
						});
					} else {
						if(!styleName) {
							throw "No style name given.";
						} else {
							throw "No style media query given.";
						}
					}
				break;

				// @<style name> [<selector> : <rule>]
				case "@":
					tokens = split(input, 2);
					var styleName = tokens[0].substring(1);
					var selector = tokens[1];
					var rule = tokens[2];

					if(styleName && selector && rule) {
						var sheet = Mindbox.StylesheetCollection.get(styleName);
						if(sheet) {
							MindboxContext.sheet = styleName;
							MindboxContext.setSelector(FirebaseSelectorTransformation.set(selector));
//							setPrompt();

							var ruleParts = rule.split(":");
							var rulePair = {};
							rulePair[ruleParts[0]] = ruleParts[1];
							
							if(ruleParts[1] === "-") {
								environment.sheets
									.child(MindboxContext.sheet)
									.child("styles")
									.child(MindboxContext.selector)
									.child(ruleParts[0]).remove();
							} else {
								environment.sheets
									.child(MindboxContext.sheet)
									.child("styles")
									.child(MindboxContext.selector)
									.child(ruleParts[0])
									.set(ruleParts[1].trim());
							}
						} else {
							throw "No stylesheet matches name '" + styleName + "'";
						}
					} else if(styleName && !selector && !rule) {
						var sheet = Mindbox.StylesheetCollection.get(styleName);
						if(!sheet) {
							throw "No stylesheet matches name '" + styleName + "'";
						}

						MindboxContext.sheet = styleName;					
					} else {
						if(!styleName) {
							throw "No style name given.";
						} else if(!selector) {
							throw "No selector given.";
						} else {
							throw "No rule given.";
						}
					}
				break;

				case "!":
					environment.commands.push("refresh");
				break;

				// [<selector>] <rule>
				default:								
					if(input.indexOf(":") === -1) {
						throw "No rule found.";
					} else {
						tokens = splitRule(input, 1);

						var left = split(tokens[0], 1);
						var rule = {}, selector;

						if(left[left.length - 1] === "") {
							left.pop();
						}

						var leftHandside = "";

						switch(left.length) {
							case 0:
								throw "Wrong rule format.";
							break;
							case 1:
								// only rule
								leftHandside = left[0];
							break;
							default:
								// selector and rule
								leftHandside = left.pop();
								selector = left.join(" ");
							break;
						}

						rule[leftHandside] = tokens[1];

						if(selector === undefined) {
							selector = MindboxContext.selector;
						}

						console.log(Mindbox);

						var sheet = Mindbox.StylesheetCollection.get(MindboxContext.sheet);
						if(sheet) {
							MindboxContext.setSelector(FirebaseSelectorTransformation.set(selector));
							//setPrompt();

							if(tokens[1] === "-") {
								environment.sheets
									.child(MindboxContext.sheet)
									.child("styles")
									.child(MindboxContext.selector)
									.child(leftHandside).remove();
							} else {
								environment.sheets
									.child(MindboxContext.sheet)
									.child("styles")
									.child(MindboxContext.selector)
									.child(leftHandside).set(tokens[1].trim());
							}
						}
					}
				break;
			} 
		};

		void function initialize() {
			Mindbox.triggers.onContextSelectorChanged.push(setPrompt);

			$(function() {
				$("html").append(
					"<div id='mindbox-console'>" + 
						"<div class='flex'>" + 
							"<div class='prompt'>&gt;</div>" + 
							"<div class='input' contenteditable></div>" +
						"</div>" + 
					"</div>");

				$(document).keypress(function(ev) {
					if(ev.which === 96) {  // tilde
						$("#mindbox-console").toggle();
						if($("#mindbox-console").is(":visible")) {
							$("#mindbox-console .input").focus();
						}

						ev.preventDefault();
						return false;
					}
				});

				$("#mindbox-console .input").keydown(function(ev) {

					if(ev.which === 13) {  // return
						var input = $("#mindbox-console .input").text();
						$("#mindbox-console .input").text("");
                                                
                        environment.history.push(FirebaseSelectorTransformation.set(input));
						interpretInput(input);
						
						ev.preventDefault();
						return false;

					} else if(ev.which === 38) {   // up
                        MindboxContext.history.current++;
                        
                        if(MindboxContext.history.current == MindboxContext.history.states.length) {
                            MindboxContext.history.current--;
                        }

                        if(MindboxContext.history.current < 0) return;
                        $("#mindbox-console .input").text(MindboxContext.history.states[MindboxContext.history.current]);

                    } else if(ev.which === 40) {	// down
                        if(MindboxContext.history.current <= 0) {
                            $("#mindbox-console .input").text("");
                            return;
                        }

                        MindboxContext.history.current--;
                        $("#mindbox-console .input").text(MindboxContext.history.states[MindboxContext.history.current]);
                    }
				});

				setPrompt();
			});			
		}();

		return {			
			eval : interpretInput,
			sheet : getCurrentSheet,
			selector : getCurrentSelector
		};
	};

	Mindbox.Console = Console;
})(Mindbox, Mindbox.Context, Mindbox.FirebaseSelectorTransformation);
