var S;

/*
 Mindbox structure
*/
var Mindbox = (function(structure) {
	"use strict";

	/*
	 Stylesheet class
	*/
	var Stylesheet = function(sheet) {

		var addStylesheetRules = function(selector, rules) {
			var sheetSize = sheet.cssRules.length;

			if("insertRule" in sheet) {
				sheet.insertRule(selector + " { " + rules + " }", sheetSize);
			} else if("addRule" in sheet) {
				sheet.addRule(selector, rules, sheetSize);
			}
		};

		var findStylesheetRule = function(selector, property) {
			var rules = sheet.cssRules;
			var sheetSize = rules.length;

			for(var i = sheetSize - 1; i >= 0; i--) {
				if(rules[i].selectorText === selector) {
					if(rules[i].cssText.indexOf(property) !== -1) {
						return rules[i];
					}
				}
			}

			return undefined;
		};

		var clearStylesheetRules = function() {
			var rules = sheet.cssRules | sheet.rules;
			var remove = sheet.removeRule | sheet.deleteRule;
			var sheetSize = rules.length;

			for(var i = sheetSize - 1; i >= 0; i--) {
				remove(i);
			}
		};

		var removeStylesheetRules = function(selector, property) {			
			var rules = sheet.cssRules;
			if(!rules) rules = sheet.rules;
			if(!rules) throw "Sheet is undefined";

			var sheetSize = rules.length;
			
			for(var i = sheetSize - 1; i >= 0; i--) {				
				if(rules[i].selectorText === selector) {
					if(!property) {
						sheet.removeRule(i);
					} else {						
						if(rules[i].cssText.indexOf(property) !== -1) {
							sheet.removeRule(i);
						}
					}
				}
			}
		};

		return {
			add : addStylesheetRules,
			find : findStylesheetRule,
			remove : removeStylesheetRules,
			clear : clearStylesheetRules
		};
	};

	/*
	 StylesheetCollection singleton
	*/
	var StylesheetCollection = (function() {
		
		var collection = {};

		var decodeMessage = function(message) {
			return {
				sheet: message.key(),
				rules: message.val()
			};
		};
		
		var createStylesheet = function(id, media) {
			var $style = document.createElement("style");
			
			$style.setAttribute("id", "style_" + id);

			if(media) {
				$style.setAttribute("media", media);
			}
			
			$style.appendChild(document.createTextNode(""));
			document.head.appendChild($style);

			collection[id] = new Stylesheet($style.sheet);
			return collection[id];
		};

		var getStylesheet = function(id) {
			return collection[id];
		};

		return {
			decode : decodeMessage,
			create : createStylesheet,
			get    : getStylesheet
		};
	})();

	/*
	 Firebase selector transformation
	*/
	var FirebaseSelectorTransformation = {
		set : function(newSelector) {
			return newSelector
				.replace(/\./gi, "@_@")
				.replace(/#/gi, "@~@")
				.replace(/\$/gi, "@%@")
				.replace(/\[/gi, "@{@")
				.replace(/\]/gi, "@}@");
		},

		get : function(selector) {
			return selector
				.replace(/\@_\@/gi, ".")
				.replace(/\@~\@/gi, "#")
				.replace(/\@%\@/gi, "$")
				.replace(/\@\{\@/gi, "[")
				.replace(/\@\}\@/gi, "]");
		}
	};

    /*
     Mindbox context singleton
    */
    var MindboxContext = { 
        sheet : "all", 
        selector : "body",
        history : {
            current: -1,    // -1 is head (current entry)
            states : []
        },

        context_selectorChanged : [],

        setSelector : function(sel) {
        	MindboxContext.selector = sel;

        	for(var index in MindboxContext.context_selectorChanged) {
        		MindboxContext.context_selectorChanged[index]();
        	}
        }
    };

	/*
	 Mindbox environment class
	*/
	var Environment = function(ref) {
		
		// Database reference
		var database = new Firebase("https://" + ref + ".firebaseio.com");
		var styling = database.child("styling");
		var structure = database.child("structure");
        var history = database.child("history");
        var commands = database.child("commands");

		var createCallbackForSheets = function(sheetName, event) {
			return function(snapshot) {				
				var element = FirebaseSelectorTransformation.get(snapshot.key());
				var properties = snapshot.val();

				var sheet = StylesheetCollection.get(sheetName);
				
				if(sheet) {
					switch(event) {
						case "added":
							for(var prop in properties) {
								var value = properties[prop];
								sheet.add(element, prop + ":" + value);
							}						
						break;
						case "changed":
							for(var prop in properties) {
								var value = properties[prop];
								sheet.add(element, prop + ":" + value);
							}						
						break;
						case "removed":						
							sheet.remove(element);
						break;
					}
				}
			};
		};

		var recursivelyUpdateLayoutStructure = function(layoutStructure, callbacks) {
			return function updateLayout(element) {
				var realElement = FirebaseSelectorTransformation.get(element);
				if(!layoutStructure || !layoutStructure[element]) return;
				var elementLayout = layoutStructure[element];

				for(var index in elementLayout) {
					var child = FirebaseSelectorTransformation.get(elementLayout[index]);
					if(callbacks.constructor === Array) {
						for(var callIndex in callbacks) {
							callbacks[callIndex](realElement, child);
						}
					} else {
						callbacks(realElement, child);
					}

					updateLayout(elementLayout[index]);
				}
			};
		}

		var environment_onStructureUpdated = [];
		var environment_onStructureRestarted = [];

		var structure_updated = function(snapshot) {
			var val = snapshot.val();
			$("body").empty();

			for(var envIndex in environment_onStructureRestarted) {
				environment_onStructureRestarted[envIndex]();
			}

			var updateLayout = recursivelyUpdateLayoutStructure(val, [ 
				function(parent, child) {
					var $parent = $(parent);
					var $child = $.jseldom(child);
					var $$child = $($child);

					$$child.attr('contenteditable', true);

					if($$child.text() === '') {
						$$child.text('Type something here.');
					}

					$parent.append($child);
				},

				function(parent, child) {
					for(var index in environment_onStructureUpdated) {
						environment_onStructureUpdated[index](parent, child);
					}
				}]);

			updateLayout("body");
		};

		var sheets_added = function(snapshot) {
			var styleName = snapshot.key();
			var styleDescription = snapshot.val();

			if(styleName && styleDescription && styleDescription.query) {
				StylesheetCollection.create(styleName, styleDescription.query);

				void function(stylesheet) {
					stylesheet.on("child_added", createCallbackForSheets(styleName, "added"));
					stylesheet.on("child_changed", createCallbackForSheets(styleName, "changed"));
					stylesheet.on("child_removed", createCallbackForSheets(styleName, "removed"));
				}(styling
					.child("sheets")
					.child(styleName)
					.child("styles"));
			}
		};

        var history_updated = function(snapshot) {
            MindboxContext.history.states.unshift(FirebaseSelectorTransformation.get(snapshot.val()));
        };

        var commands_updated = function(snapshot) {
            var val = snapshot.val();
            switch(val) {
                case "refresh":
                    snapshot.ref().remove();
                    window.location.reload();                    
                break;
            }
        };

		void function initialize() {
			history.on('child_added', history_updated);

            structure.on('value', structure_updated);

            commands.on('child_added', commands_updated);

			void function(sheets) {
				sheets.on("child_added", sheets_added);
			}(styling.child("sheets"));
		}();

		return {
			sheets : styling.child("sheets"),
			structure : structure,
            history : history,
            commands : commands,

            triggers : {
	            onStructureUpdated : environment_onStructureUpdated,
	            onStructureRestarted : environment_onStructureRestarted
	        }
		};
	};

	return {
		StylesheetCollection : StylesheetCollection,
		Environment : Environment,
		Context : MindboxContext,
		FirebaseSelectorTransformation : FirebaseSelectorTransformation,

		triggers : {
			onContextSelectorChanged : MindboxContext.context_selectorChanged
		}
	};
})({});